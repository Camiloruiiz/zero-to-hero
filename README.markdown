# Project: "Zero to Hero Web Developer"

Date: 24 Nov 2016  
Autor: Camilo Ruiz  
Dead-line first versión: 14 Dic 2016  
Finished first version: 1 Feb 2017  
Last Update: 24 Nov 2017


#### Targets
1.	Make more fast the process to develop webs.
 	* Practise to build websites from scrash.
 	* Get more speed, writing more.
2. Have more control and organization with the projects.
3. Build my own develop flow.
4. Develop a good taks in gulp integrating wpack
5. Develop my own started theme.

#### Secundary Targets
1. Practise git.
2. Practise vin.
3. Retake the programing world.


#### Gold Rules:

- Keep simple, do not use more than you need.


#### DevDependecies

-"require-dir"
//For more control I'll use the package "require-dir", This allows me to split the code for task in diferents folder and have all the config in only one file.
-"gulp"
-"browser-sync"

#### Instalation
Clone the repository on your computer and change into the projects folder.
Run:
```sh
$ npm install
```

**Hint**: If you get errors while installing `gulp-imagemin` it may help to execute this command before running `npm install`:

```sh
export PKG_CONFIG_PATH=/opt/X11/lib/pkgconfig
```

**Hint**: I recently ran into problem with `gulp-imagemin`. This may be an error with my current version of node. But if you get the same error on `gulp publish`, while optimizing images install manually these files:

```sh
$ npm install optipng-bin
$ npm install cwebp-bin
```

#### Setup

Open `gulp/config.js` and change settings if needed. Only the `rsync` settings need to be adjusted. Change the `destination` to a path on your webserver and change `hostname` and `username`.

#### Running Gulp.js

Three tasks are available:

```sh
$ gulp
```
- Running `gulp` will start a development server, build assets and the site and start a `watch` task.


___
###### Tools Used:
- Gulp
- Sass


###### NOTES

- gulfile.js = File were we'll write all the config for run gulp.
- package.json = File were we'll write all our necessary pakages.

