import gulp    from 'gulp'
import jshint  from 'gulp-jshint'
import stylish from 'jshint-stylish'
var config  = require('../config').js.jshint;

/**
 * Check JavaScript sytax with JSHint
 */
gulp.task('jshint', function() {
  return gulp.src(config.src)
    .pipe(jshint())
    .pipe(jshint.reporter(stylish));
});
