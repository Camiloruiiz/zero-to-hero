import path from 'path';
import webpack from 'webpack';
import process from 'process';
import HtmlWebpackPlugin from 'html-webpack-plugin';

const isProduction = (process.env.NODE_ENV === 'production');

const UglifyJSPlugin = require('uglifyjs-webpack-plugin')

export let config = {

    entry: {
        scripts: [
            './_assets/javascripts/application.js',
            './_assets/javascripts/bootstrap.js',
        ],
    },
    output: {
        path: path.resolve(__dirname, '../../site.com/src/'),
        filename: "./static/js/[name].js"
    },
    module: {
        loaders: [{
            include: /\.pug/,
            loader: ['raw-loader', 'pug-html-loader'],            
        }]
    },
    resolve: {
        alias: {
            'masonry': 'masonry-layout',
            'isotope': 'isotope-layout',
            'magnific-popup': 'magnific-popup'
        }
    },


    context: path.resolve(__dirname, '../../site.com/src'),

    plugins: isProduction ? [
        new UglifyJSPlugin(),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery',
            Popper: ['popper.js', 'default'],
            Flickity: "flickity-bg-lazyload"
        }),
        new HtmlWebpackPlugin({
            inject: false,
            filename: '../public_html/index.html',
            template: './index.pug'
        })
        
    ] : [
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery',
            Popper: ['popper.js', 'default'],
            Flickity: "flickity-bg-lazyload"
        }),
        new HtmlWebpackPlugin({
            inject: false,
            template: './index.pug'
        })
    ]

};

module.exports = { config };