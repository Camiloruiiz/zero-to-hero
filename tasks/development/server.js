import gulp        from 'gulp'
import browserSync from 'browser-sync'
import webpack from 'webpack'
import webpackDevMiddleware from 'webpack-dev-middleware'
import { config as webpackConfig } from './webpack'

const server = browserSync.create('development')
const config = require('../config').browsersync.development;

const bundler = webpack(webpackConfig)

export function reload(done) {
    browserSync.get('development').reload();
    done();
}

export function serve(done) {
    config.middleware = webpackDevMiddleware(bundler, { quiet: true });
    server.init(config);
    done();
}
