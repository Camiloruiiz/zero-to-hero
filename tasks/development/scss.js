import gulp         from 'gulp'
import rucksack     from 'rucksack-css'
import autoprefixer from 'autoprefixer'
import gutil        from 'gulp-util'
import sourcemaps   from 'gulp-sourcemaps'
import reporter     from 'postcss-reporter'
import stylelint    from 'stylelint'
import browser      from 'browser-sync'
import { serve }    from './server'

import gulpLoadPlugins from 'gulp-load-plugins';
 
const $      = gulpLoadPlugins();

const config = require('../config').css.development;

//var  gulpFilter = require('gulp-filter');


const browserSync = browser.get('development') 

function onError (err) {
  
  var err_message = "<div style='color:red; text-align: center;'><p>" + err.messageOriginal + "<br> LINE: " + err.line + "<br> FILE: " + err.file + "<br></p></div>";
  
  gutil.beep();
  browserSync.notify(err_message, 100000);
  console.log(err);
  this.emit('end');
}
/**
 * Rund CSS through PostCSS and it's plugins
 * Build sourcemaps and minimize
 */

//var filter = gulpFilter(['*.css', '!*.map'], {restore: true});

export function scss(done) {
  
  // Stylelint config rules
  let stylelintConfig = {
    "rules": {
      "selector-no-id": true
    }
  };
  
  let processors = [
    autoprefixer(config.options.autoprefixer),
    rucksack(), // generate responsive fonts
    //stylelint(stylelintConfig),
    reporter({
      clearMessages: true,
      throwError: true
    })
  ];
  
  browserSync.notify("Compiling Sass, please wait!", 2000);
      
	return gulp.src(config.src)
    .pipe($.plumber({
      errorHandler: onError
    }))
    .pipe(sourcemaps.init())
    .pipe($.sass({outputStyle: 'expanded'}))
    .pipe($.postcss(processors))
    //.pipe(filter) // Don’t write sourcemaps of sourcemaps
    .pipe(sourcemaps.write('./', { includeContent: true, sourceRoot: '../../_assets/scss/'}))	
    //.pipe(filter.restore) // Restore original files
  	.pipe(gulp.dest(config.dest))
    .pipe(browserSync.stream({match: '**/*.css'}));
  done();
};

