import gulp            from 'gulp'
import gulpLoadPlugins from 'gulp-load-plugins';

const $      = gulpLoadPlugins();
const config = require('../config').copy.fonts;

/**
 * Copy Fonts
 */
export function fonts(done) {
	return gulp.src(config.src)
        .pipe($.newer(config.dest)) // Ignore unchanged files
        .pipe(gulp.dest(config.dest));
    done();
};
