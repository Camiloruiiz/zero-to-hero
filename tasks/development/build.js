import gulp from 'gulp';
import { serve, reload } from './server.js';
import { scss } from './scss.js';
import { clean } from './clean.js';
import { images } from './images.js';
import { fonts } from './fonts.js';

const config = require('../config').watch.development;


const watch = (done) => {
    gulp.watch(config.pug, reload);
    gulp.watch(config.scss, gulp.series(scss, 'scsslint'));
    gulp.watch(config.scripts, gulp.series('jshint', reload));
    gulp.watch(config.images, gulp.series(images));
    gulp.watch(config.fonts, gulp.series(fonts));
    done();
};

gulp.task(
    'build:dev',
    gulp.series(
        clean,
        gulp.parallel(
            scss
        ),
        images,
        fonts,
        serve,
        watch
    )
);
