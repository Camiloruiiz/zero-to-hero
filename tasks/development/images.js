import gulp            from 'gulp';
import gulpLoadPlugins from 'gulp-load-plugins';

const $      = gulpLoadPlugins();
const config = require('../config').images;

/**
 * Copy images to build folder
 * if not changed
 */
export function images(done) {
  var s = $.size();
  
  return gulp.src(config.src)
    .pipe(s)
    .pipe($.newer(config.dest)) // Ignore unchanged files
    .pipe(gulp.dest(config.dest))
    .pipe($.notify({
  		title: 'Copy all Images',
      subtitle: 'Total size',
  		onLast: true,
      wait: true,
  		message: function () {
  		    	return s.prettySize;
  		}
	}));
  done();
};
