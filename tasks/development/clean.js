import gulp   from 'gulp'
import del    from 'del'
var config = require('../config').delete.development

/**
 * Delete folders and files
 */
export function clean(done) {
	del(config.src, {force: true});
	done();
};
