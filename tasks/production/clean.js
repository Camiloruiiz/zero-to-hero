import gulp from 'gulp'
import del from 'del'
const config = require('../config').delete.production;

/**
 * Clean output directory
 */
export const clean = done => del(config.src, { force: true, dot: true}, done);