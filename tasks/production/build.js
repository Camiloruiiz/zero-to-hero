import gulp              from 'gulp'
import { serve, reload } from './browser-sync.js'
import { css }           from './css.js'
import { clean }         from './clean.js'
import { images }        from './img.js'
import { scripts }       from './scripts.js'
import { revision }      from './revision.js'
import { revcollect }    from './rev-collector.js'
import { copy, copyjs }  from './copy.js'
import { rename }        from './rename.js'


const config = require('../config').watch.production;

const watch = (done) => {
    gulp.watch(config.css, gulp.series(css));
    done();
};

gulp.task(
    'build',
    gulp.series(
        clean,
        copy,
        gulp.parallel(
            css,
            scripts,
            images
        ),
        copyjs,
        rename,
        serve,
        watch
    )
);

gulp.task(
    'rev',
    gulp.series(
        revision,
        revcollect
    )
);