import gulp            from 'gulp'
import browser         from "browser-sync"
import gulpLoadPlugins from 'gulp-load-plugins';

const $           = gulpLoadPlugins();
const config      = require('../config').copy;
const browsersync = browser.get('production')

/**
 * Build the Jekyll Site
 */
export function copy(done) {
    browsersync.notify('Compiling (Production)');
	return gulp.src(config.src)
        .pipe($.newer(config.dest)) // Ignore unchanged files
        .pipe(gulp.dest(config.dest));
    done();
};

/**
 * Copy JS files
 */
export function copyjs() {
	return gulp.src(config.js.src)
        .pipe($.rename({ suffix: '.min' }))
        .pipe(gulp.dest(config.js.dest))
};
