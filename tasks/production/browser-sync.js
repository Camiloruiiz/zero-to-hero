import gulp        from 'gulp'
import browserSync from 'browser-sync'
const server = browserSync.create('production')
const config = require('../config').browsersync.production;

export function reload(done) {
    browserSync.get('production').reload();
    done();
}

export function serve(done) {
	server.init(config);
	done();
}
