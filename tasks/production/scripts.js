import webpack    from 'webpack';
import { config } from '../development/webpack';

export function scripts() {
    return new Promise(resolve => webpack(config, (err, stats) => {
        if (err) console.log('Webpack', err);
        console.log(stats.toString("errors-only"));
        resolve();
    }));
}
