import gulp   from 'gulp'
import rev    from 'gulp-rev'
import revDel from 'rev-del'
const config = require('../config').revision.wp;

/**
 * Revision all asset files and
 * write a manifest file
 */
export function revision() {
  return gulp.src(config.src.assets, { base: config.src.base })
    .pipe(gulp.dest(config.dest.assets))
    .pipe(rev())
    .pipe(gulp.dest(config.dest.assets))
    .pipe(rev.manifest({
    	base: './',
    	merge: true 
	}))
	.pipe(revDel()) 
    .pipe(gulp.dest(config.dest.manifest.path));
};