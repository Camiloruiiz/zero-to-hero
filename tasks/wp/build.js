import gulp              from 'gulp'
import { serve, reload, clear } from './browser-sync.js'
import { css }           from './css.js'
//import { clean }         from './clean.js'
//import { images }        from './img.js'
//import { scripts }       from './scripts.js'
import { revision }      from './revision.js'
import { revcollect }    from './rev-collector.js'
//import { copy, copyjs }  from './copy.js'
//import { rename }        from './rename.js'


const config = require('../config').watch.wp;

const watch = (done) => {
    gulp.watch(config.css, gulp.series('rev:wp', reload));
    done();
};

gulp.task(
    'rev:wp',
    gulp.series(
        revision,
        revcollect
    )
);

gulp.task(
    'wp',
    gulp.series(
        'rev:wp',
        serve,
        watch
    )
);

