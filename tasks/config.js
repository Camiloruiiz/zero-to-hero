const site = 'site.com';
const src = site + '/src';
const srcAssets = src + '/_assets';
const developmentAssets = src + '/static';
const build = site + '/public_html';
const productionAssets = build + '/static';
const wp = site + '/app/public/wp-content/themes/_s-child';
const wpAssets = wp + '/static';

import process from 'process';

const USER_ENV = process.env.USER_ENV;

module.exports = {
    browsersync: {
        development: {
            server: {
                baseDir: src + '/',
            },
            middleware: [],
            port: 8080,
            files: src + '/*.html'
        },
        production: {
            server: {
                baseDir: build + '/',
            },
            port: 8081
        },
        wp: {
            proxy: 'site.com.dev/',
            injectChanges: true,
            //tunnel: true,
            //tunnel: "ppress",
            port: 8082,
            files: wpAssets + '/css/main.min.css'
        }
    },
    delete: {
        development: {
            src: [developmentAssets + '/**/*']
        },
        production: {
            src: [
                productionAssets + '/css/**',
                productionAssets + '/js/**',
                build + '/**/*.{php,html}'
            ]
        }
    },
    watch: {
        development: {
            pug: src + '/**/*.{pug}',
            scss: srcAssets + '/scss/**/*{sass,scss}',
            images: srcAssets + '/images/**/*',
            scripts: srcAssets + '/javascripts/**/*.js',
            sprites: srcAssets + '/images/**/*.png',
            svg: 'vectors/*.svg',
            fonts: srcAssets + '/fonts/**'
        },
        production: {
            css: developmentAssets + '/css/**/*{css}'
        },
        wp: {
            css: productionAssets + '/css/**/*{css}'
        }
            
    },
    copy: {
        fonts: {
            src: [
                srcAssets + '/fonts/**'
            ],
            dest: developmentAssets + '/fonts/'
        },
        js: {
            src: [
                src + '/static/js/*.js'
            ],
            dest: productionAssets + '/js/'
        },
        src: [
            'node_modules/apache-server-configs/dist/.htaccess',
            //src + '/_includes/**/*.{html,php}',
            src + '/**/*',
            //src + '/.htaccess',
            '!' + src + '/_assets{,/**}',
            '!' + src + '/_bower_components{,/**}',
            '!' + src + '/static/css{,/**}',
            '!' + src + '/_includes{,/**}',
            '!' + src + '/*.{pug}',
            '!' + src + '/static/js{,/**}'
        ],
        dest: build
    },
    css: {
        development: {
            src: srcAssets + '/scss/*.scss',
            dest: developmentAssets + '/css',
            options: {
                autoprefixer: {
                    browsers: [
                        '> 0.3%',
                        'last 2 versions',
                        'ie >= 10',
                        'ie_mob >= 10',
                        'ff >= 30',
                        'chrome >= 34',
                        'Firefox ESR',
                        'opera >= 23',
                        'iOS >= 6',
                        'android >= 4.4',
                        'bb >= 10',
                        'safari >= 5'
                    ],
                    cascade: true
                },
                mqpacker: {}
            }
        },
        production: {
            src: developmentAssets + '/css/*.css',
            dest: productionAssets + '/css',
        },
        wp: {
            src: developmentAssets + '/css/*.css',
            dest: wpAssets + '/css',
        }
    },
    js: {
        jshint: {
            src: srcAssets + '/javascripts/*.js'
        },
        production: {
            src: [developmentAssets + '/js/*.js'],
            dest: productionAssets + '/js/',
            options: {}
        }
    },
    images: {
        src: srcAssets + '/images/**/*.{jpg,jpeg,png,gif,ico,html,JPG,svg}',
        dest: developmentAssets + '/images/',
        options: {
            optimizationLevel: 3,
            progessive: true,
            interlaced: true,
            svgoPlugins: [{ removeViewBox: false }]
        },
        production: {
            src: developmentAssets + '/images/**/*.{jpg,jpeg,png,gif,ico,JPG}',
            dest: productionAssets + '/images/',
            options: {
                optimizationLevel: 3,
                progessive: true,
                interlaced: true,
                svgoPlugins: [{ removeViewBox: false }]
            }
        }
    },
    html: {
        src: build + '/**/*.html',
        dest: build,
        options: {
            removeComments: true,
            collapseWhitespace: true,
            collapseBooleanAttributes: true,
            removeAttributeQuotes: true,
            removeRedundantAttributes: true,
            removeEmptyAttributes: true,
            removeScriptTypeAttributes: true,
            removeStyleLinkTypeAttributes: true,
            removeOptionalTags: true
        }
    },
    rename: {
        production: {
            src: build + '/*.html',
            dest: build,
            remplace: {
                js: {
                    x: /(js\/(application|scripts|head))\.js/g,
                    y: '$1.min.js'
                },
                css: {
                    x: /(css\/(main))\.css/g,
                    y: '$1.min.css'
                }
            }
        },
    },
    revision: {
        src: {
            assets: [
                productionAssets + '/css/*.css',
                productionAssets + '/js/*.js'
                //productionAssets + '/images/**/*'
            ],
            base: productionAssets
        },
        dest: {
            assets: productionAssets,
            manifest: {
                name: 'manifest.json',
                path: productionAssets
            }
        },
        wp: {
            src: {
                assets: [
                    productionAssets + '/css/*.css',
                    //productionAssets + '/js/*.js'
                    //productionAssets + '/images/**/*'
                ],
                base: productionAssets + '/css/'
            },
            dest: {
                assets: wpAssets + '/css/',
                manifest: {
                    name: 'manifest.json',
                    path: wpAssets + '/css/'
                }
            }
        }
    },
    collect: {
        src: [
            productionAssets + '/rev-manifest.json',
            build + '/**/*.{html,php}'
        ],
        dest: build,
        wp: {
            src: [
                wpAssets + '/css/rev-manifest.json',
                wpAssets + '/style.css'
            ],
            dest: wp
        }
    },
    rsync: {
        src: build + '/**',
        options: {
            destination: '~/etheme.camiloruiz.co.uk/public_html',
            root: build,
            hostname: 'home683465163.1and1-data.host',
            username: USER_ENV,
            incremental: true,
            progress: true,
            relative: true,
            emptyDirectories: true,
            recursive: true,
            clean: true,
            exclude: ['.DS_Store'],
            include: []
        }
    },
    sitemap: {
        src: build + '/**/*.html',
        dest: build
    }
};
