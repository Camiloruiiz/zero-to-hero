import gulp from 'gulp';
import requireDir from 'require-dir';
import { savePsiReport } from './util/pagespeed';
requireDir('./', { recurse: true });

export const dev   = gulp.series( 'build:dev' );
export const wp   = gulp.series( 'wp' );
export const build = gulp.series( 'build' , 'sitemap' );
export const deploy = gulp.series( 'rsync', 'ping' );
export const seo = gulp.series( 'mobile', 'desktop', savePsiReport, 'ping');

export default dev;
